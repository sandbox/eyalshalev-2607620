<?php
/**
 * @file
 * Contains Drupal\graph\Entity\EntityGraphInterface
 */


namespace Drupal\graph\Entity;

use Drupal\Core\Entity\EntityInterface;

/**
 * @class EntityGraphInterface
 */
interface EntityGraphInterface extends EntityInterface {

  /**
   * Constructs and returns the graph representation of graph entity.
   * @return \Drupal\graph\GraphInterface
   */
  public function toGraph();

  /**
   * @param \Drupal\graph\Entity\EntityVertexInterface $entity_vertex
   * @param bool $reverse
   *  If TRUE then this method will return all the entities vertices that
   *  point to the given entity vertex.
   * @return mixed
   */
  public function getEdges(EntityVertexInterface $entity_vertex, $reverse = FALSE);

  /**
   * Returns the edge map lambda that translates a entity vertex into the vertices that it points to.
   * @return callable
   */
  public function edgeMap();

  /**
   * @return \Drupal\graph\Entity\EntityVertexInterface[]
   */
  public function vertices();
}