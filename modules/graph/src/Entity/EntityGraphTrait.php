<?php
/**
 * @file
 * Contains Drupal\graph\Entity\EntityGraphTrait
 */


namespace Drupal\graph\Entity;

use Drupal\graph\Graph;

/**
 * @class EntityGraphTrait
 */
trait EntityGraphTrait {

  /**
   * @var \Drupal\graph\GraphInterface
   */
  protected $graph;

  /**
   * @return \Drupal\graph\GraphInterface
   */
  public function toGraph() {
    if (empty($this->graph)) {
      $vertices = array_map(function(EntityVertexInterface $entity_vertex) {
        return $entity_vertex->toVertex();
      }, $this->vertices());
      $this->graph = new Graph($vertices, [$this, 'edgeMap']);
    }
    return $this->graph;
  }

}