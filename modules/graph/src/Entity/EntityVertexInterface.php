<?php
/**
 * @file
 * Contains Drupal\graph\Entity\EntityVertexInterface
 */


namespace Drupal\graph\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graph\VertexInterface;

/**
 * @interface EntityVertexInterface
 */
interface EntityVertexInterface extends EntityInterface {

  /**
   * @return string
   */
  public function value();

  /**
   * @return VertexInterface
   */
  public function toVertex();

  /**
   * @param bool $reverse
   * @return EntityVertexInterface[]
   */
  public function edges($reverse = FALSE);
}