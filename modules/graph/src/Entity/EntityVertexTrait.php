<?php
/**
 * @file
 * Contains Drupal\graph\Entity\EntityVertexTrait
 */


namespace Drupal\graph\Entity;
use Drupal\graph\Vertex;


/**
 * @trait EntityVertexTrait
 */
trait EntityVertexTrait {

  /**
   * @var \Drupal\graph\VertexInterface
   */
  protected $vertex;

  /**
   * {@inheritdoc}
   */
  public function toVertex() {
    if (empty($this->vertex)) {
      $this->vertex = new Vertex($this->value());
    }
    return $this->vertex;
  }

  /**
   * {@inheritdoc}
   */
  public function value() {
    return $this->getEntityKey('value');
  }

}