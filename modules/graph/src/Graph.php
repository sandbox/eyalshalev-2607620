<?php
/**
 * @file
 * Contains Drupal\graph\Graph
 */

namespace Drupal\graph;

use Drupal\graph\Algorithm\Bfs;
use Drupal\graph\Algorithm\Tarjan;

/**
 * @class Graph
 */
class Graph implements GraphInterface {

  /**
   * @var \Drupal\graph\VertexInterface[]
   */
  protected $vertices = [];

  /**
   * @var callable|\Closure
   */
  protected $edgeMap;

  /**
   * @var \Drupal\graph\Algorithm\Tarjan
   */
  protected $tarjan;

  /**
   * @var \Drupal\graph\Algorithm\Bfs[]
   */
  protected $bfs = [];

  /**
   * Graph constructor.
   * @param \Drupal\graph\VertexInterface[] $vertices
   * @param callable|\Closure $edge_map
   */
  public function __construct($vertices, callable $edge_map = NULL) {
    foreach ($vertices as $vertex) {
      $this->vertices[$vertex->value()] = $vertex;
    }
    if (empty($edge_map)) {
      $edge_map = function () {
        return [];
      };
    }
    $this->edgeMap = $edge_map;
  }

  /**
   * Creates a full edge map for a given graph.
   *
   * @param \Drupal\graph\GraphInterface $graph
   * @return callable
   */
  public static function getFullEdgeMapOf(GraphInterface $graph) {
    return function () use ($graph) {
      return $graph->getVertices();
    };
  }

  /**
   * {@inheritdoc}
   */
  public function isFullyLoaded() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function get($value) {
    return $this->vertices[$value];
  }

  /**
   * {@inheritdoc}
   */
  public function set(VertexInterface $vertex) {
    $vertices[$vertex->value()] = $vertex;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function contains($value) {
    return array_key_exists($value, $this->vertices);
  }

  /**
   * {@inheritdoc}
   */
  public function bfs(VertexInterface $root, $sort = FALSE, $max_depth = INF) {
    if (empty($this->bfs[$root->value()])) {
      $this->bfs[$root->value()] = new Bfs($this, $root);
      $sort = TRUE;
    }
    if ($sort || $this->bfs[$root->value()]->getMaxDepth() < $max_depth) {
      $this->bfs[$root->value()]->apply($max_depth);
    }
    return $this->bfs[$root->value()];
  }

  /**
   * {@inheritdoc}
   */
  public function getEdges(VertexInterface $v) {
    $edgeMap = $this->getEdgeMap();
    return $edgeMap($v, $this);
  }

  /**
   * {@inheritdoc}
   */
  public function getEdgeMap() {
    return $this->edgeMap;
  }

  /**
   * {@inheritdoc}
   */
  public function getVertices(array $values = NULL) {
    if (is_null($values)) {
      return $this->vertices;
    }
    return array_intersect_key($this->vertices, array_flip($values));
  }

  /**
   * {@inheritdoc}
   */
  public function getSortedVertices() {
    return $this->tarjan()->getList();
  }

  /**
   * {@inheritdoc}
   */
  public function tarjan($sort = FALSE) {
    if (empty($this->tarjan)) {
      $this->tarjan = new Tarjan($this);
      $sort = TRUE;
    }
    if ($sort) {
      $this->tarjan->apply();
    }
    return $this->tarjan;
  }

  /**
   * {@inheritdoc}
   */
  public function isTree() {
    return $this->isForest() && $this->tarjan()->isStronglyConnected();
  }

  /**
   * {@inheritdoc}
   */
  public function isForest() {
    return !$this->tarjan()->hasLoopback();
  }

  /**
   * @param \Drupal\graph\VertexInterface[] $vertices
   * @return GraphInterface
   */
  public function generateSubGraph(array $vertices) {
    $parentGraph = $this;
    return new Graph($vertices, function (VertexInterface $vertex) use ($parentGraph, $vertices) {
      $parentEdgeMap = $parentGraph->getEdgeMap();
      $parentEdges = $parentEdgeMap($vertex);
      $vertices_uuid = array_map(function (VertexInterface $vertex) {
        return $vertex->value();
      }, $vertices);
      return array_filter($parentEdges, function (VertexInterface $vertex) use ($vertices_uuid) {
        return in_array($vertex->value(), $vertices_uuid);
      });
    });
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return count($this->vertices);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet($offset) {
    return $this->get($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet($offset, $value) {
    if ($this->contains($offset)) {
      $vertex = $this->get($offset);
    }
    else {
      $vertex = new Vertex($value);
    }
    $this->set($vertex);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset($offset) {
    unset($this->vertices[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists($offset) {
    return $this->contains($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize() {
    return [
      'vertices' => $this->vertices,
      'edgeMatrix' => $this->getEdgeMapAsMatrix()
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function serialize() {
    return serialize($this->jsonSerialize());
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize($serialized) {
    $data = unserialize($serialized);
    $this->vertices = $data['vertices'];
    $this->edgeMap = static::edgeMapFromMatrix($this, $data['edgeMatrix']);
  }

  /**
   * Builds a matrix of the edges of this graph
   *
   * @return VertexInterface[]
   */
  public function getEdgeMapAsMatrix() {
    $graph = $this;
    return array_map(function(VertexInterface $vertex) use ($graph) {
      return $graph->getEdges($vertex);
    }, $this->vertices);
  }

  /**
   * Builds an edge map lambda based on a given graph and an edge matrix.
   *
   * @param \Drupal\graph\GraphInterface $graph
   * @param array $edge_matrix
   * @return \Closure
   */
  public static function edgeMapFromMatrix(GraphInterface $graph, array $edge_matrix) {
    return function(VertexInterface $vertex) use ($graph, $edge_matrix) {
      return array_map([$graph, 'get'], $edge_matrix[$vertex->value()]);
    };
  }
}