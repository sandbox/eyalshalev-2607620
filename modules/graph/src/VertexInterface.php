<?php
/**
 * @file
 * Contains Drupal\graph\VertexInterface
 */

namespace Drupal\graph;

interface VertexInterface extends \Serializable, \JsonSerializable {

  /**
   * A UUID for this Vertex
   *
   * @return string
   */
  public function value();

}