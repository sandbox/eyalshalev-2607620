<?php
/**
 * @file
 * Contains Drupal\graph\LazyGraph
 */


namespace Drupal\graph;


/**
 * @class LazyGraph
 */
class LazyGraph extends Graph {

  protected $unloaded_vertices = [];

  public function __construct(array $unloaded_vertices, callable $edge_map = NULL, $algorithms = NULL) {
    parent::__construct([], $edge_map, $algorithms);
    $this->unloaded_vertices = $unloaded_vertices;
  }

  /**
   * {@inheritdoc}
   */
  public function isFullyLoaded() {
    return !empty($unloaded_vertices);
  }

  /**
   * {@inheritdoc}
   */
  public function contains($value) {
    return array_key_exists($value, $this->unloaded_vertices) || parent::contains($value);
  }

  /**
   * {@inheritdoc}
   */
  public function getVertices(array $values = NULL) {
    $vertices_to_load = $this->unloaded_vertices;
    if (!is_null($values)) {
      $vertices_to_load = array_intersect_key($vertices_to_load, array_flip($values));
    }
    foreach (array_keys($vertices_to_load) as $uuid) {
      $this->get($uuid);
    }
    return parent::getVertices($values);
  }

  /**
   * {@inheritdoc}
   */
  public function get($value) {
    if (empty($this->vertices[$value])) {
      /** @var callable $callable */
      $callable = $this->unloaded_vertices[$value];
      $this->vertices[$value] = $callable();
      unset($this->unloaded_vertices[$value]);
    }
    return parent::get($value);
  }
}