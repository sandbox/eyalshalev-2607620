<?php
/**
 * @file
 * Contains Drupal\graph\Forest
 */


namespace Drupal\graph;


/**
 * @class Forest
 */
class Forest extends GraphWrapper implements ForestInterface {

  protected $roots = [];

  /**
   * @param \Drupal\graph\GraphInterface $original
   * @return \Drupal\graph\ForestInterface
   */
  public static function create(GraphInterface $original) {
    if ($original->isForest()) {
      return parent::create($original);
    }
    else {
      throw new \InvalidArgumentException('The received graph is circular.', static::GRAPH_IS_CIRCULAR);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRoots() {
    $components_uuids = array_keys($this->tarjan()->getComponents());
    $graph = $this;
    return array_map(function ($uuid) use ($graph) {
      return $graph->get($uuid);
    }, $components_uuids);
  }

  /**
   * {@inheritdoc}
   */
  public function shortestPath(VertexInterface $root, VertexInterface $vertex) {
    return $this->bfs($root)->shortestPath($vertex);
  }

  /**
   * {@inheritdoc}
   */
  public function each($callback) {
    static::_each($this->tarjan()->getComponents(), $callback, $this);
    return $this;
  }

  protected static function _each(array $targets, $callback, ForestInterface $forest) {
    foreach ($targets as $uuid => $children) {
      $callback($forest->get($uuid));
      Forest::_each($children, $callback, $forest);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function map($callback) {
    return static::_map($this->tarjan()->getComponents(), $callback, $this);
  }

  /**
   * {@inheritdoc}
   */
  public static function _map(array $targets, $callback, ForestInterface $forest, array $parents = []) {
    $map = [];
    foreach ($targets as $uuid => $children) {
      $current = $forest->get($uuid);
      $map[$uuid]['root'] = $callback($current, $parents, !empty($children));
      $parents[] = $current;
      $map[$uuid]['children'] = Forest::_map($children, $callback, $forest, $parents);
    }
    return $map;
  }

  public function reduce($callback, $initial) {
    return static::_reduce($this->tarjan()
      ->getComponents(), $callback, $initial, [], $this);
  }

  /**
   * {@inheritdoc}
   */
  public static function _reduce(array $targets, $callback, $carry, array $path = [], ForestInterface $forest) {
    foreach ($targets as $uuid => $children) {
      $current = $forest->get($uuid);
      $carry = $callback($carry, $current, $path, !empty($children));
      $path[] = $current;
      $carry = Forest::_reduce($children, $callback, $carry, $path, $forest);
    }
    return $carry;
  }
}