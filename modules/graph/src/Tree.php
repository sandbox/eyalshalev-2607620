<?php
/**
 * @file
 * Contains Drupal\graph\Tree
 */


namespace Drupal\graph;


/**
 * @class Tree
 */
class Tree extends Forest implements TreeInterface {

  /**
   * @param \Drupal\graph\GraphInterface $original
   * @return \Drupal\graph\TreeInterface
   * @throws \InvalidArgumentException
   *   If the received graph is not structured as a tree
   */
  public static function create(GraphInterface $original) {
    if ($original->isTree()) {
      return parent::create($original);
    }
    elseif ($original->isForest()) {
      throw new \InvalidArgumentException('The received graph has multiple roots', static::GRAPH_HAS_MULTIPLE_ROOTS);
    }
    else {
      throw new \InvalidArgumentException('The received graph is circular', static::GRAPH_IS_CIRCULAR);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRoot() {
    return current($this->getRoots());
  }
}