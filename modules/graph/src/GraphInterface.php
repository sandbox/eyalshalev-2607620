<?php
/**
 * @file
 * Contains Drupal\graph\GraphInterface
 */


namespace Drupal\graph;


/**
 * @class GraphInterface
 */
interface GraphInterface extends \Countable, \ArrayAccess, \Serializable, \JsonSerializable {

  /**
   * Is this graph a lazy graph?
   * @return bool
   */
  public function isFullyLoaded();

  /**
   * Searches for the vertex with the given uuid in the graph.
   * @param $value
   * @return \Drupal\graph\VertexInterface
   */
  public function get($value);

  /**
   * Is there a vertex with the given uuid in the graph
   * @param $value
   * @return bool
   */
  public function contains($value);

  /**
   * @param VertexInterface $root
   * @param bool $sort
   * @param int $max_depth
   * @return \Drupal\graph\Algorithm\Bfs
   */
  public function bfs(VertexInterface $root, $sort = FALSE, $max_depth = INF);

  /**
   * @param bool $sort
   *  Should the graph be sorted?
   * @return \Drupal\graph\Algorithm\Tarjan
   */
  public function tarjan($sort = FALSE);

  /**
   * @return callable|\Closure
   */
  public function getEdgeMap();

  /**
   * @param \Drupal\graph\VertexInterface $v
   * @return \Drupal\graph\VertexInterface[]
   */
  public function getEdges(VertexInterface $v);

  /**
   * @return \Drupal\graph\VertexInterface[]
   */
  public function getVertices();

  /**
   * @return bool
   */
  public function isTree();

  /**
   * @return bool
   */
  public function isForest();

  /**
   * @param \Drupal\graph\VertexInterface[] $vertices
   * @return GraphInterface
   */
  public function generateSubGraph(array $vertices);
}