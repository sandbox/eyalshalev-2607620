<?php
/**
 * @file
 * Contains Drupal\graph\TreeInterface
 */


namespace Drupal\graph;


interface TreeInterface extends ForestInterface {
  const GRAPH_HAS_MULTIPLE_ROOTS = -2;

  /**
   * @return \Drupal\graph\VertexInterface
   */
  public function getRoot();
}