<?php
/**
 * @file
 * Contains Drupal\graph\GraphWrapper
 */


namespace Drupal\graph;


/**
 * @class GraphWrapper
 */
class GraphWrapper implements GraphInterface {

  /**
   * @var \Drupal\graph\GraphInterface
   *   The original graph
   */
  protected $original;

  protected function __construct(GraphInterface $original) {
    $this->original = $original;
  }

  /**
   * @param \Drupal\graph\GraphInterface $original
   * @return static
   */
  public static function create(GraphInterface $original) {
    return new static($original);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists($offset) {
    return $this->original->offsetExists($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet($offset) {
    return $this->original->offsetGet($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet($offset, $value) {
    $this->original->offsetSet($offset, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset($offset) {
    $this->original->offsetUnset($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function serialize() {
    return $this->original->serialize();
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize($serialized) {
    $this->original = unserialize($serialized);
  }

  /**
   * {@inheritdoc}
   */
  public function isFullyLoaded() {
    return $this->original->isFullyLoaded();
  }

  /**
   * {@inheritdoc}
   */
  public function get($value) {
    return $this->original->get($value);
  }

  /**
   * {@inheritdoc}
   */
  public function contains($value) {
    return $this->original->contains($value);
  }

  /**
   * {@inheritdoc}
   */
  public function bfs(VertexInterface $root, $sort = FALSE, $max_depth = INF) {
    return $this->original->bfs($root, $sort, $max_depth);
  }

  /**
   * {@inheritdoc}
   */
  public function tarjan($sort = FALSE) {
    return $this->original->tarjan($sort);
  }

  /**
   * {@inheritdoc}
   */
  public function getEdgeMap() {
    return $this->original->getEdgeMap();
  }

  /**
   * {@inheritdoc}
   */
  public function getEdges(VertexInterface $v) {
    return $this->original->getEdges($v);
  }

  /**
   * {@inheritdoc}
   */
  public function getVertices() {
    return $this->original->getVertices();
  }

  /**
   * {@inheritdoc}
   */
  public function isTree() {
    return $this->original->isTree();
  }

  /**
   * {@inheritdoc}
   */
  public function isForest() {
    return $this->original->isForest();
  }

  /**
   * {@inheritdoc}
   */
  public function generateSubGraph(array $vertices) {
    $this->original->generateSubGraph($vertices);
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return $this->original->count();
  }

  /**
   * {@inheritdoc}
   */
  function jsonSerialize() {
    return $this->original->jsonSerialize();
  }
}