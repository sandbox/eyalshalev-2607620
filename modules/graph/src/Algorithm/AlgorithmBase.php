<?php
/**
 * @file
 * Contains Drupal\graph\Algorithm\AlgorithmBase
 */


namespace Drupal\graph\Algorithm;

use Drupal\graph\GraphInterface;

/**
 * @class AlgorithmBase
 */
abstract class AlgorithmBase implements AlgorithmInterface {

  protected $applied = FALSE;

  /**
   * @var \Drupal\graph\Graph
   * The graph object that this algorithm will apply onto.
   */
  protected $graph;

  public function __construct(GraphInterface $graph) {
    $this->graph = $graph;
  }

  /**
   * Sets the applied flag of the algorithm
   * @param bool $applied
   * @return $this
   */
  protected function setApplied($applied = TRUE) {
    $this->applied = $applied;
    return $this;
  }

  public function isApplied() {
    return $this->applied;
  }

  /**
   * {@inheritdoc}
   * An empty implementation of the Apply method.
   * @see doApply()
   */
  public function apply(...$args) {
    $this->setApplied(TRUE);
    call_user_func_array([$this, 'doApply'], $args);
    return $this;
  }

  /**
   * The actual apply method.
   */
  protected abstract function doApply();
}