<?php
/**
 * @file
 * Contains Drupal\graph\Algorithm\AlgorithmInterface
 */


namespace Drupal\graph\Algorithm;


/**
 * @class AlgorithmInterface
 */
interface AlgorithmInterface {

  /**
   * Applies the algorithm on the given graph
   *
   * @param array ...$args
   * @return $this
   */
  public function apply(...$args);
}