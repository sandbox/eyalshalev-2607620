<?php
/**
 * @file
 * Contains Drupal\graph\Algorithm\Tarjan
 */

namespace Drupal\graph\Algorithm;
use Drupal\graph\VertexInterface;

/**
 * @class Tarjan implements Tarjan's algorithm for Topological sorting
 * @see https://en.wikipedia.org/wiki/Topological_sorting#Tarjan.27s_algorithm
 */
class Tarjan extends AlgorithmBase {

  /**
   * @var VertexInterface[]
   */
  protected $list = [];

  /**
   * @var bool
   */
  protected $calculated = FALSE;

  /**
   * @var bool
   */
  protected $loopback;

  /**
   * @var \SplObjectStorage
   */
  protected $unmarked;

  /**
   * @var \SplObjectStorage
   */
  protected $tempedMarked;

  /**
   * @var array
   */
  protected $components;

  /**
   * If a loopback is detected it means that the graph is cyclic.
   * @return bool
   */
  public function hasLoopback() {
    if (!$this->isApplied()) {
      $this->apply();
    }
    return $this->loopback;
  }

  /**
   * @return bool
   */
  public function isStronglyConnected() {
    if (!$this->isApplied()) {
      $this->apply();
    }
    return count($this->components) <= 1;
  }

  /**
   * @return \Drupal\graph\VertexInterface[][]
   */
  public function getComponents() {
    if (!$this->isApplied()) {
      $this->apply();
    }
    return $this->components;
  }

  /**
   * The graph vertices in a typological ordering.
   *
   * @return VertexInterface[]
   */
  public function getList() {
    if (!$this->isApplied()) {
      $this->apply();
    }
    return $this->list;
  }

  /**
   * {@inheritdoc}
   */
  public function doApply() {
    // Initialize members
    $this->list = [];
    $this->unmarked = new \SplObjectStorage();
    foreach ($this->graph->getVertices() as $vertex) {
      $this->unmarked->attach($vertex);
    }
    $this->unmarked->rewind();
    $this->tempedMarked = new \SplObjectStorage();
    $this->components = [];

    $loopback = FALSE;
    while ($this->unmarked->count() > 0) {
      /** @var VertexInterface $current */
      $current = $this->unmarked->current();
      $this->components[$current->value()] = [];
      $loopback = $this->visit($current, [$current->value()]) || $loopback;
      $this->unmarked->next();
      $this->unmarked->rewind();
    }

    $this->loopback = $loopback;
    $this->calculated = TRUE;

    return $this;
  }

  /**
   * @param \Drupal\graph\VertexInterface $v
   * @param array $path
   * @return bool was a loopback detected?
   */
  private function visit(VertexInterface $v, array $path) {
    if ($this->tempedMarked->contains($v)) {
      // Visiting a temporally marked edge means a loopback occurred.
      return TRUE;
    }
    elseif ($this->unmarked->contains($v)) {
      $this->tempedMarked->attach($v);

      $tarjan = $this;
      // Visit all the edges and combine the results.
      /** @var bool $loopback */
      $loopback = array_reduce($this->graph->getEdges($v), function ($carry, VertexInterface $edge) use ($tarjan, $path) {
        $children = &$tarjan->components;
        foreach ($path as $item) {
          $children = &$children[$item];
        }
        $children[$edge->value()] = [];
        $path[] = $edge->value();
        return $this->visit($edge, $path) || $carry;
      }, FALSE);

      $this->unmarked->detach($v);
      $this->tempedMarked->detach($v);
      $this->list[] = $v;
      return $loopback;
    }
    else {
      return TRUE;
    }
  }
}