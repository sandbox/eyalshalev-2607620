<?php
/**
 * @file
 * Contains Drupal\graph\Algorithm\Bfs
 */


namespace Drupal\graph\Algorithm;

use Drupal\graph\GraphInterface;
use Drupal\graph\VertexInterface;

/**
 * @class Bfs
 */
class Bfs extends AlgorithmBase {

  /**
   * @var \Drupal\graph\VertexInterface
   * The BFS algorithm calculates the ancestry and distances starting with a
   * single vertex.
   * This is the root vertex.
   */
  protected $root_vertex;

  /**
   * @var int
   * The maximum depth that the BFS last drilled down with.
   */
  protected $maxDepth = INF;

  /**
   * @var int[]
   * The distance of all the vertices from the root vertex.
   */
  protected $distance = [];

  /**
   * @var \Drupal\graph\VertexInterface[]
   */
  protected $parent = [];

  /**
   * @var \Drupal\graph\Algorithm\Bfs[]
   */
  protected static $instance = [];

  /**
   * @param \Drupal\graph\GraphInterface $graph
   *   The Graph object that the BFS will apply on.
   * @param \Drupal\graph\VertexInterface $root_vertex
   *   The BFS algorithm calculates the ancestry and distances starting with the
   *   root vertex.
   */
  public function __construct(GraphInterface $graph, VertexInterface $root_vertex) {
    parent::__construct($graph);
    $this->root_vertex = $root_vertex;
  }

  /**
   * The maximum depth that the BFS last drilled down with.
   * @return int
   */
  public function getMaxDepth() {
    return $this->maxDepth;
  }

  /**
   * @param \Drupal\graph\VertexInterface $target
   * @return bool
   *   True if the target vertex exists in the subtree of the root vertex.
   *   False otherwise.
   */
  public function inTree(VertexInterface $target) {
    if (!$this->isApplied()) {
      $this->apply();
    }
    return INF > $this->distance[$target->value()];
  }

  /**
   * Build and return the shortest path from the root vertex to the target vertex
   * based on the last applied BFS.
   * @param \Drupal\graph\VertexInterface $target
   *  The target vertex
   * @return \Drupal\graph\VertexInterface[]|NULL
   *  An array of vertices that create a path from the root vertex to the target
   *  vertex. Or null if the target vertex is not on tree of the root.
   */
  public function shortestPath(VertexInterface $target) {
    if (!$this->isApplied()) {
      $this->apply();
    }
    if ($this->inTree($target)) {
      $reverse_path = [];
      $current = $target;
      while (NULL !== ($current = $this->parent[$current->value()])) {
        $reverse_path[] = $current;
      }
      return $reverse_path;
    }
    else {
      return NULL;
    }
  }

  /**
   * @return \Drupal\graph\GraphInterface
   */
  public function toSubgraph() {
    if (!$this->isApplied()) {
      $this->apply();
    }
    $vertices = array_filter($this->distance, function ($distance) {
      return $distance !== INF;
    });
    $vertices = array_intersect_key($this->graph->getVertices(), $vertices);
    return $this->graph->generateSubGraph($vertices);
  }

  /**
   * {@inheritdoc}
   */
  public function doApply($max_depth = FALSE) {
    if (FALSE !== $max_depth) {
      $this->maxDepth = $max_depth;
    }
    $this->distance = [];
    $this->parent = [];
    foreach ($this->graph->getVertices() as $vertex) {
      $this->distance[$vertex->value()] = INF;
      $this->parent[$vertex->value()] = NULL;
    }
    $queue = new \SplQueue();
    $this->distance[$this->root_vertex->value()] = 0;
    $queue->enqueue($this->root_vertex);

    while ($queue->count() > 0) {
      /** @var VertexInterface $current */
      $current = $queue->dequeue();

      if ($this->distance[$current->value()] < $this->maxDepth) {
        foreach ($this->graph->getEdges($current) as $edge) {
          if ($this->distance[$edge->value()] === INF) {
            $this->distance[$edge->value()] = 1 + $this->distance[$current->value()];
            $this->parent[$edge->value()] = $current;
            $queue->enqueue($edge);
          }
        }
      }

    }

    return $this;
  }
}