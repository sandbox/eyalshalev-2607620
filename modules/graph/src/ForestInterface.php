<?php
/**
 * @file
 * Contains Drupal\graph\ForestInterface
 */

namespace Drupal\graph;

interface ForestInterface extends GraphInterface {
  const GRAPH_IS_CIRCULAR = -1;

  /**
   * @return \Drupal\graph\VertexInterface[]
   */
  public function getRoots();

  /**
   * @param \Drupal\graph\VertexInterface $root
   * @param \Drupal\graph\VertexInterface $vertex
   * @return \Drupal\graph\VertexInterface[]
   */
  public function shortestPath(VertexInterface $root, VertexInterface $vertex);

  /**
   * @param callable $callback
   */
  public function each($callback);

  /**
   * Example usage:
   * <code>
   *   $tree->map(function(VertexInterface $vertex, VertexInterface $parent, $has_children) {
   *
   *   });
   * </code>
   *
   * @param callable $callback
   * @return array
   */
  public function map($callback);

  /**
   * @param callable $callback
   * @param mixed $initial
   * @return mixed
   */
  public function reduce($callback, $initial);
}