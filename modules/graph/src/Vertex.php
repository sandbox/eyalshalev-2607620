<?php
/**
 * @file
 * Contains Drupal\graph\Vertex
 */


namespace Drupal\graph;

/**
 * @class Vertex
 */
class Vertex implements VertexInterface {

  /**
   * @var string
   */
  protected $value;

  /**
   * Vertex constructor.
   * @param string $value
   */
  public function __construct($value) {
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getEdges(GraphInterface $graph) {
    $edgeMap = $graph->getEdgeMap();
    return $edgeMap($this);
  }

  /**
   * {@inheritdoc}
   */
  public function serialize() {
    serialize($this->jsonSerialize());
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize() {
    return [
      'value' => $this->value()
    ];
  }

  public function value() {
    return $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize($serialized) {
    $json_serialized = unserialize($serialized);
    $this->value = $json_serialized['value'];
  }
}