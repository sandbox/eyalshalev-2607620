<?php
/**
 * @file
 * Contains Drupal\term\Controller\TermListController
 */


namespace Drupal\term\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * @class TermListController
 */
class TermListController extends ControllerBase {

  /**
   * @var \Drupal\term\TermListBuilder
   */
  protected $termListBuilder;

  public function __construct(EntityListBuilderInterface $term_list_builder) {
    $this->termListBuilder = $term_list_builder;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.term.list_builder')
    );
  }

  /**
   * Provides the listing page for any entity type.
   *
   * @param string $vocabulary_id
   *   The entity type to render.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function listing($vocabulary_id) {
    $this->termListBuilder->setVocabularyId($vocabulary_id);
    return $this->termListBuilder->render();
  }
}