<?php

/**
 * @file
 * Contains Drupal\term\Controller\TermAddController.
 */

namespace Drupal\term\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\term\VocabularyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class TermAddController.
 *
 * @package Drupal\term\Controller
 */
class TermAddController extends ControllerBase {
  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $vocabularyStorage;

  public function __construct(EntityStorageInterface $storage, EntityStorageInterface $type_storage) {
    $this->termStorage = $storage;
    $this->vocabularyStorage = $type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.term.storage'),
      $container->get('entity.vocabulary.storage')
    );
  }

  /**
   * Displays add links for available bundles/types for entity term .
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A render array for a list of the term bundles/types that can be added or
   *   if there is only one type/bunlde defined for the site, the function returns the add page for that bundle/type.
   */
  public function add(Request $request) {
    $vocabularies = $this->vocabularyStorage->loadMultiple();
    if ($vocabularies && count($vocabularies) == 1) {
      $vocabulary = reset($vocabularies);
      return $this->addForm($vocabulary, $request);
    }
    if (count($vocabularies) === 0) {
      return array(
        '#type' => 'inline_template',
        '#template' => '{% trans %}You have not created any {{ bundle }} yet.{% endtrans %} {{ add_bundle_link | render }}',
        '#context' => [
          'bundle' => $this->vocabularyStorage->getEntityType()
            ->getLowercaseLabel(),
          'add_bundle_link' => Link::fromTextAndUrl($this->t('Click to create a new @bundle', [
            '@bundle' => $this->vocabularyStorage->getEntityType()
              ->getLowercaseLabel()
          ]), Url::fromRoute('entity.vocabulary.add_form'))
            ->toRenderable()
        ],
//        '#markup' => $this->t('You have not created any %bundle types yet. @link to add a new type.', [
//          '%bundle' => 'Term',
//          '@link' => Link::fromTextAndUrl($this->t('Go to the type creation page'), Url::fromRoute('entity.vocabulary.add_form'))
//            ->toString(),
//        ]),
      );
    }
    return array(
      '#theme' => 'term_content_add_list',
      '#content' => $vocabularies
    );
  }

  /**
   * Presents the creation form for term entities of given bundle/type.
   *
   * @param VocabularyInterface $vocabulary
   *   The custom bundle to add.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A form array as expected by drupal_render().
   */
  public function addForm(VocabularyInterface $vocabulary, Request $request) {
    $entity = $this->termStorage->create(array(
      $vocabulary->getTermEntityType()->getKey('bundle') => $vocabulary->id()
    ));
    return $this->entityFormBuilder()->getForm($entity);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param EntityInterface $vocabulary
   *   The custom bundle/type being added.
   *
   * @return string
   *   The page title.
   */
  public function getAddFormTitle(VocabularyInterface $vocabulary) {
    return t('Create of bundle @label',
      array('@label' => $vocabulary->label())
    );
  }

}
