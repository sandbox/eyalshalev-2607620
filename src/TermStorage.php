<?php
/**
 * @file
 * Contains Drupal\term\TermStorage
 */


namespace Drupal\term;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * @class TermStorage
 */
class TermStorage extends SqlContentEntityStorage implements TermStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByValue($value) {
    $terms = $this->loadByProperties([
      $this->getEntityType()->getKey('value') => $value
    ]);
    if (count($terms) > 0) {
      return reset($terms);
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   * @return \Drupal\term\TermInterface|NULL
   */
  public function load($id) {
    return parent::load($id);
  }

  /**
   * {@inheritdoc}
   * @return \Drupal\term\TermInterface[]
   */
  public function loadMultiple(array $ids = NULL) {
    return parent::loadMultiple($ids);
  }
}