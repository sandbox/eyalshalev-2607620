<?php

/**
 * @file
 * Contains \Drupal\term\Form\VocabularyForm.
 */

namespace Drupal\term\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\term\Entity\Term;
use Drupal\term\VocabularyInterface;

/**
 * Class VocabularyForm.
 *
 * @package Drupal\term\Form
 */
class VocabularyForm extends EntityForm {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $vocabulary = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $vocabulary->label(),
      '#description' => $this->t("Label for the Vocabulary."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $vocabulary->id(),
      '#machine_name' => [
        'exists' => '\Drupal\term\Entity\Vocabulary::load',
      ],
      '#disabled' => !$vocabulary->isNew(),
    ];

    return $form;
  }

  /**
   * @return \Drupal\term\VocabularyInterface
   */
  public function getEntity() {
    return parent::getEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $vocabulary = $this->getEntity();
    $status = $vocabulary->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Vocabulary.', [
          '%label' => $vocabulary->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Vocabulary.', [
          '%label' => $vocabulary->label(),
        ]));
    }
    $form_state->setRedirectUrl($vocabulary->toUrl('collection'));
  }

}
