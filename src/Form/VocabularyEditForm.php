<?php

/**
 * @file
 * Contains \Drupal\term\Form\VocabularyEditForm.
 */

namespace Drupal\term\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\graph\Tree;
use Drupal\graph\VertexInterface;
use Drupal\term\VocabularyInterface;

/**
 * Class VocabularyForm.
 *
 * @package Drupal\term\Form
 */
class VocabularyEditForm extends VocabularyForm {
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label']['#disabled'] = TRUE;
    $form['id']['#disabled'] = TRUE;

    $form['terms'] = [
      '#type' => 'details',
      '#title' => $this->t('Term edge map'),
      '#open' => TRUE,
      '#tree' => TRUE
    ];

    $terms = $this->getEntity()->loadTerms();
    foreach ($terms as $value => $term) {
      $other_terms = array_diff_key($terms, [$value => $term]);
      $edges = $this->getEntity()->getEdgesAsValues($term);
      $form['terms'][$value] = [
        '#type' => 'select',
        '#title' => $term->label(),
        '#multiple' => TRUE,
        '#options' => $other_terms,
        '#default_value' => $edges
      ];
    }

    $graph = $this->getEntity()->toGraph();
    if ($graph->isTree()) {
      $tree = Tree::create($graph);

      $form['tree'] = $tree->reduce(function ($carry, VertexInterface $vertex, $ancestors, $has_children) {
        $element = &$carry;
        /** @var VertexInterface[] $ancestors */
        foreach ($ancestors as $ancestor) {
          $element = &$element[$ancestor->value()];
        }
        $element[$vertex->value()] = [
          '#type' => $has_children ? 'fieldset' : 'item',
          '#title' => $vertex->value(),
        ];
        return $carry;
      }, [
        '#type' => 'details',
        '#title' => $this->t('Tree')
      ]);
    }

    return $form;
  }

  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit_and_add_term'] = $actions['submit'];
    $actions['submit_and_add_term']['#value'] = $this->t('Save and add terms.');
    $actions['submit_and_add_term']['#submit'][] = '::redirectToTermAdd';
    return $actions;
  }

  public function redirectToTermAdd(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.term.add_form', [
      'vocabulary' => $this->getEntity()
        ->id()
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    /** @var VocabularyInterface $entity */
    $entity = parent::buildEntity($form, $form_state);

    $raw_terms = $form_state->getValue('terms');
    $raw_terms = !empty($raw_terms) ? $raw_terms : [];
    $terms = [];
    foreach ($raw_terms as $value => $edges) {
      $terms[] = [
        'value' => $value,
        'edges' => array_values($edges)
      ];
    }

    $entity->set('terms', $terms);
    return $entity;
  }

}
