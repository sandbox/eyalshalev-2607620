<?php

/**
 * @file
 * Contains \Drupal\term\Form\TermForm.
 */

namespace Drupal\term\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;

/**
 * Form controller for Term edit forms.
 *
 * @ingroup term
 */
class TermForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $term = $this->getEntity();
    $entity_type = $term->getEntityType();

    $form['langcode'] = array(
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $term->langcode(),
      '#languages' => Language::STATE_ALL,
    );

    return $form;
  }

  /**
   * @return \Drupal\term\TermInterface
   */
  public function getEntity() {
    return parent::getEntity();
  }

  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit']['#submit'][] = '::redirectToVocabulary';

    $actions['submit_and_add_term'] = $actions['submit'];
    $actions['submit_and_add_term']['#value'] = $this->t('Save and add another term.');
    $actions['submit_and_add_term']['#submit'][] = '::redirectToTermAdd';

    return $actions;
  }

  public function redirectToTermAdd(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.term.add_form', [
      'vocabulary' => $this->getEntity()
        ->bundle()
    ]);
  }

  public function redirectToVocabulary(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.vocabulary.edit_form', [
      $this->getEntity()
        ->getEntityType()
        ->getBundleEntityType() => $this->getEntity()->bundle()
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Term.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Term.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.term.edit_form', ['term' => $entity->id()]);
  }

}
