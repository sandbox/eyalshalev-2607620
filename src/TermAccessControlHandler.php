<?php

/**
 * @file
 * Contains \Drupal\term\TermAccessControlHandler.
 */

namespace Drupal\term;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Term entity.
 *
 * @see \Drupal\term\Entity\Term.
 */
class TermAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished term entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published term entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit term entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete term entities');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add term entities');
  }

}
