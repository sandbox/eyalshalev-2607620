<?php
/**
 * @file
 * Contains Drupal\term\TermStorageInterface
 */


namespace Drupal\term;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\graph\TreeInterface;

interface TermStorageInterface extends ContentEntityStorageInterface {
  const MULTIPLE_ROOT_TERMS = TreeInterface::GRAPH_HAS_MULTIPLE_ROOTS;
  const VOCABULARY_IS_CIRCULAR = TreeInterface::GRAPH_IS_CIRCULAR;

  /**
   * @param $value
   * @return \Drupal\term\TermInterface|NULL
   */
  public function loadByValue($value);
}