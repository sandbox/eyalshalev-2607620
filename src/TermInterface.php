<?php

/**
 * @file
 * Contains \Drupal\term\TermInterface.
 */

namespace Drupal\term;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\graph\Entity\EntityVertexInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Term entities.
 *
 * @ingroup term
 */
interface TermInterface extends ContentEntityInterface, EntityVertexInterface {

  public function langcode();

  /**
   * @return \Drupal\Core\Entity\EntityTypeInterface
   */
  public function getVocabularyEntityType();

  /**
   * @return \Drupal\Core\Entity\EntityStorageInterface
   */
  public function getVocabularyStorage();

  /**
   * Gets the Vocabulary.
   *
   * @return \Drupal\term\VocabularyInterface
   *   The Vocabulary.
   */
  public function getVocabulary();

  /**
   * Gets the Term creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Term.
   */
  public function getCreatedTime();

  /**
   * Sets the Term creation timestamp.
   *
   * @param int $timestamp
   *   The Term creation timestamp.
   *
   * @return \Drupal\term\TermInterface
   *   The called Term entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Term published status indicator.
   *
   * Unpublished Term are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Term is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Term.
   *
   * @param bool $published
   *   TRUE to set this Term to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\term\TermInterface
   *   The called Term entity.
   */
  public function setPublished($published);

  /**
   * @return \Drupal\term\TermInterface[]
   */
  public function parents();

  /**
   * @return \Drupal\term\TermInterface[]
   */
  public function children();

}
