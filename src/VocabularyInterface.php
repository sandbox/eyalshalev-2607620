<?php

/**
 * @file
 * Contains \Drupal\term\VocabularyInterface.
 */

namespace Drupal\term;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\graph\Entity\EntityGraphInterface;
use Drupal\graph\Entity\EntityVertexInterface;

/**
 * Provides an interface for defining Vocabulary entities.
 */
interface VocabularyInterface extends ConfigEntityInterface, EntityGraphInterface {

  const TREE = 1;
  const FOREST = 2;
  const CIRCULAR = 3;

  /**
   * @return string
   */
  public function getTermEntityTypeId();

  /**
   * @return \Drupal\Core\Entity\EntityTypeInterface
   */
  public function getTermEntityType();

  /**
   * @return \Drupal\term\TermStorageInterface
   */
  public function getTermStorage();

  /**
   * @return \Drupal\term\TermInterface[]
   */
  public function loadTerms();

  /**
   * @return array
   */
  public function edgeMatrix();

  /**
   * @param \Drupal\graph\Entity\EntityVertexInterface $term
   * @return string[]
   */
  public function getEdgesAsValues(EntityVertexInterface $term);

  /**
   * Returns the vocabulary hierarchy.
   *
   * @return int
   *   The vocabulary hierarchy.
   */
  public function getHierarchy();

  /**
   * Finds all terms in a given vocabulary ID.
   *
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave INF to return all
   *   levels.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The root term under $vid (or the provided $parent)
   */
  public function loadTreeRoot($max_depth = INF);

  /**
   * Finds all terms in a given vocabulary ID.
   *
   * @param int $max_depth
   *   The number of levels of the tree to return. Leave INF to return all
   *   levels.
   *
   * @return \Drupal\taxonomy\TermInterface[]
   *   The root terms
   */
  public function loadForestRoots($max_depth = INF);
}
