<?php

/**
 * @file
 * Contains \Drupal\term\TermListBuilder.
 */

namespace Drupal\term;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Term entities.
 *
 * @ingroup term
 */
class TermListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;

  /**
   * @var string
   */
  protected $vocabularyId = NULL;

  public function setVocabularyId($vocabulary_id) {
    $this->vocabularyId = $vocabulary_id;
  }

  public function getQuery() {
    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if (!empty($this->vocabularyId)) {
      $query->condition($this->entityType->getKey('bundle'), $this->vocabularyId);
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query;
  }

  public function getEntityIds() {
    return $this->getQuery()->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Term ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\term\Entity\Term */
    $row['id'] = $entity->id();
    $row['name'] = $entity->toLink();
    return $row + parent::buildRow($entity);
  }

}
