<?php

/**
 * @file
 * Contains \Drupal\term\Entity\Term.
 */

namespace Drupal\term\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\graph\Entity\EntityVertexTrait;
use Drupal\term\TermInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Term entity.
 *
 * @ingroup term
 *
 * @ContentEntityType(
 *   id = "term",
 *   label = @Translation("Term"),
 *   bundle_label = @Translation("Vocabulary"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\term\TermListBuilder",
 *     "views_data" = "Drupal\term\Entity\TermViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\term\Form\TermForm",
 *       "add" = "Drupal\term\Form\TermForm",
 *       "edit" = "Drupal\term\Form\TermForm",
 *       "delete" = "Drupal\term\Form\TermDeleteForm",
 *     },
 *     "access" = "Drupal\term\TermAccessControlHandler",
 *     "storage" = "Drupal\term\TermStorage",
 *   },
 *   base_table = "term",
 *   admin_permission = "administer Term entities",
 *   entity_keys = {
 *     "bundle" = "vocabulary",
 *     "id" = "id",
 *     "value" = "value",
 *     "label" = "value",
 *     "uuid" = "uuid",
 *     "uid" = "owner",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *     "changed" = "changed",
 *     "created" = "created"
 *   },
 *   links = {
 *     "collection" = "/term",
 *     "canonical" = "/term/{term}",
 *     "edit-form" = "/term/{term}/edit",
 *     "delete-form" = "/term/{term}/delete"
 *   },
 *   bundle_entity_type = "vocabulary",
 *   field_ui_base_route = "entity.vocabulary.edit_form"
 * )
 */
class Term extends ContentEntityBase implements TermInterface, EntityChangedInterface, EntityOwnerInterface {

  use EntityChangedTrait;
  use EntityVertexTrait;

  /**
   * @var \Drupal\graph\VertexInterface
   */
  protected $vertex;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      $storage_controller->getEntityType()
        ->getKey('uid') => \Drupal::currentUser()->id(),
    );
  }

  /**
   * Helper function.
   *
   * Used in array_map calls
   *
   * @param \Drupal\term\TermInterface $term
   * @return \Drupal\graph\VertexInterface
   */
  public static function termToVertex(TermInterface $term) {
    return $term->toVertex();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];
    foreach (static::keyFieldDefinitions($entity_type) as $key => $fieldDefinition) {
      $fields[$entity_type->getKey($key)] = $fieldDefinition;
    }
    return $fields;
  }

  public function langcode() {
    return $this->getEntityKey('langcode');
  }

  /**
   * @return string
   */
  public function __toString() {
    return $this->value();
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyEntityType() {
    return \Drupal::entityTypeManager()->getDefinition($this->getEntityType()
      ->getBundleEntityType());
  }

  /**
   * {@inheritdoc}
   */
  public function edges($reverse = FALSE) {
    return $this->getVocabulary()->getEdges($this, $reverse);
  }

  /**
   * {@inheritdoc}
   */
  public function parents() {
    return $this->edges(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function children() {
    return $this->edges();
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabulary() {
    return $this->getVocabularyStorage()->load($this->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyStorage() {
    return \Drupal::entityTypeManager()->getStorage($this->getEntityType()
      ->getBundleEntityType());
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
    $field = $this->get($this->getEntityType()->getKey('uid'));
    return current($field->referencedEntities());
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->getEntityKey('uid');
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set($this->getEntityType()->getKey('uid'), $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set($this->getEntityType()->getKey('uid'), $account);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  protected static function keyFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Term entity.'))
      ->setReadOnly(TRUE);
    $fields['bundle'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The Vocabulary/bundle.'))
      ->setSetting('target_type', 'vocabulary')
      ->setRequired(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Term entity.'))
      ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Term entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['value'] = BaseFieldDefinition::create('string')
      ->addConstraint('UniqueField', [])
      ->setRequired(TRUE)
      ->setLabel(t('Value'))
      ->setDescription(t('The term value.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Term is published.'))
      ->setDefaultValue(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Term entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
