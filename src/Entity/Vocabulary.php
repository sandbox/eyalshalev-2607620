<?php

/**
 * @file
 * Contains \Drupal\term\Entity\Vocabulary.
 */

namespace Drupal\term\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\graph\Entity\EntityGraphTrait;
use Drupal\graph\Entity\EntityVertexInterface;
use Drupal\graph\Forest;
use Drupal\graph\VertexInterface;
use Drupal\term\TermInterface;
use Drupal\term\VocabularyInterface;

/**
 * Defines the Vocabulary entity.
 *
 * @ConfigEntityType(
 *   id = "vocabulary",
 *   label = @Translation("Vocabulary"),
 *   handlers = {
 *     "list_builder" = "Drupal\term\VocabularyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\term\Form\VocabularyForm",
 *       "edit" = "Drupal\term\Form\VocabularyEditForm",
 *       "delete" = "Drupal\term\Form\VocabularyDeleteForm"
 *     }
 *   },
 *   config_prefix = "vocabulary",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "term",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/vocabulary/{vocabulary}",
 *     "edit-form" = "/admin/structure/vocabulary/{vocabulary}/edit",
 *     "delete-form" = "/admin/structure/vocabulary/{vocabulary}/delete",
 *     "collection" = "/admin/structure/visibility_group"
 *   }
 * )
 */
class Vocabulary extends ConfigEntityBundleBase implements VocabularyInterface {

  use EntityGraphTrait;

  /**
   * The Vocabulary ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Vocabulary label.
   *
   * @var string
   */
  protected $label;

  /**
   * @var array
   */
  protected $terms = [];

  /**
   * @var array
   */
  protected $matrix = [];

  /**
   * @var \Drupal\term\TermInterface[]
   *   The root terms
   * @see \Drupal\term\VocabularyInterface::loadForestRoots()
   */
  protected $roots = [];

  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
  }

  /**
   * @param string $key
   * @return string
   */
  protected function getTermKey($key) {
    return $this->getTermEntityType()->getKey($key);
  }

  /**
   * @return \Drupal\Core\Entity\Query\QueryInterface
   */
  protected function getTermQuery() {
    return $this->getTermStorage()->getQuery()
      ->condition($this->getTermEntityType()->getKey('bundle'), $this->id());
  }

  /**
   * {@inheritdoc}
   * @return \Drupal\term\VocabularyInterface|null
   */
  public static function load($id) {
    return parent::load($id);
  }

//  /**
//   * {@inheritdoc}
//   */
//  public function toGraph($lazy_load = FALSE) {
//    if (empty($this->graph)) {
//      $matrix = $this->matrix;
//      $edge_map = function (VertexInterface $vertex, GraphInterface $graph) use ($matrix) {
//        $edges = array_key_exists($vertex->value(), $matrix) ? $matrix[$vertex->value()] : [];
//        return $graph->getVertices($edges);
//      };
//      if ($lazy_load) {
//        $term_storage = $this->getTermStorage();
//        $vertices = [];
//        foreach ($matrix as $value => $edges) {
//          $vertices[$value] = function() use ($value, $term_storage) {
//            return $term_storage->loadByValue($value)->toVertex();
//          };
//        }
//
//        $this->graph = new LazyGraph($vertices, $edge_map);
//      }
//      else {
//        $vertices = array_map([
//          Term::class,
//          'termToVertex'
//        ], $this->loadTerms());
//        $this->graph = new Graph($vertices, $edge_map);
//      }
//    }
//    return $this->graph;
//  }

  /**
   * {@inheritdoc}
   */
  public function getTermStorage() {
    return $this->entityTypeManager()
      ->getStorage($this->getTermEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function getTermEntityTypeId() {
    return $this->getEntityType()->getBundleOf();
  }

  /**
   * {@inheritdoc}
   */
  public function loadTerms() {
    $terms = $this->getTermStorage()->loadByProperties([
      $this->getTermKey('bundle') => $this->id()
    ]);
    $terms_by_id = [];
    foreach ($terms as $term) {
      $terms_by_id[$term->id()] = $term;
    }
    return $terms_by_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTermEntityType() {
    return $this->entityTypeManager()
      ->getDefinition($this->getTermEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function edgeMatrix() {
    if (empty($this->matrix)) {
      $this->matrix = [];
      foreach ($this->terms as $term) {
        $this->matrix[$term['value']] = [];
        foreach ($term['edges'] as $edge) {
          $this->matrix[$term['value']][] = $edge;
        }
      }
    }
    return $this->matrix;
  }

  /**
   * {@inheritdoc}
   */
  public function getEdges(EntityVertexInterface $term, $reverse = FALSE) {
    $values = $this->getEdgesAsValues($term);
    if (empty($values)) {
      return [];
    }
    else {
      $query = $this->getTermStorage()->getQuery('OR');
      foreach ($values as $value) {
        $query->condition($this->getTermKey('label'), $value);
      }
      return $this->getTermStorage()->loadMultiple($query->execute());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEdgesAsValues(EntityVertexInterface $term, $reverse = FALSE) {
    if ($reverse) {
      $terms = array_filter($this->terms, function ($term_vertex) use ($term) {
        return in_array($term->label(), $term_vertex['edges']);
      });
      $values = array_map(function ($term_vertex) {
        return $term_vertex['value'];
      }, $terms);
      return array_values($values);
    }
    else {
      $term_vertices = array_filter($this->terms, function ($term_vertex) use ($term) {
        return $term_vertex['value'] === $term->label();
      });
      if (empty($term_vertices)) {
        return [];
      }
      else {
        $term_vertex = current($term_vertices);
        $term_vertex += ['edges' => []];
        return $term_vertex['edges'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function vertices() {
    return $this->loadTerms();
  }

  /**
   * {@inheritdoc}
   */
  public function edgeMap() {
    $matrix = $this->edgeMatrix();
    return function (VertexInterface $v) use ($matrix) {
      return array_map(function (EntityVertexInterface $edge) {
        return $edge->toVertex();
      }, $matrix[$v->value()]);
    };
  }

  /**
   * {inheritdoc}
   */
  public function getHierarchy() {
    $graph = $this->toGraph();
    if ($graph->isTree()) {
      return static::TREE;
    }
    elseif ($graph->isForest()) {
      return static::FOREST;
    }
    else {
      return static::CIRCULAR;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadTreeRoot($max_depth = INF) {
    $graph = $this->toGraph();

    if ($graph->isTree()) {
      $roots = $this->loadForestRoots();
      return reset($roots);
    }
    elseif ($graph->isForest()) {
      throw new \LogicException('The vocabulary contains multiple roots', static::FOREST);
    }
    else {
      throw new \LogicException('The vocabulary is circular', static::CIRCULAR);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadForestRoots($max_depth = INF) {
    $graph = $this->toGraph();

    if ($graph->isForest()) {
      if (empty($this->roots)) {
        $this->roots = [];
        $forest = Forest::create($graph);
        if (!empty($vertex_roots = $forest->getRoots())) {
          $ids = $this->termVertexQuery($vertex_roots)->execute();
          $this->roots = $this->getTermStorage()->loadMultiple($ids);
        }
      }
      return $this->roots;
    }
    else {
      throw new \LogicException('The vocabulary is circular', static::CIRCULAR);
    }
  }

  /**
   * @param \Drupal\graph\Entity\EntityVertexInterface[] $vertices
   * @return \Drupal\Core\Entity\Query\QueryInterface
   */
  protected function termVertexQuery(array $vertices) {
    $query = $this->getTermQuery();
    $group = $query->orConditionGroup();
    $value_field = $this->getTermEntityType()->getKey('value');
    foreach ($vertices as $vertex) {
      $group->condition($value_field, $vertex->value());
    }
    $query->condition($group);
    return $query;
  }

  public function loadShortestPath(TermInterface $source, TermInterface $target) {
    $graph = $this->toGraph();
    $path = $graph->bfs($source->toVertex())->shortestPath($target->toVertex());
    if (is_null($path)) {
      throw new \LogicException("There is no path from {$source->value()} to {$target->value()}");
    }
    elseif(empty($path)) {
      return [];
    }
    else {
      $query = $this->termVertexQuery($path);
      return $this->getTermStorage()->loadMultiple($query->execute());
    }
  }
}
