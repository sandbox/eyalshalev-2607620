<?php

/**
 * @file
 * Contains \Drupal\term\Entity\Term.
 */

namespace Drupal\term\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Term entities.
 */
class TermViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['term']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Term'),
      'help' => $this->t('The Term ID.'),
    );

    return $data;
  }

}
